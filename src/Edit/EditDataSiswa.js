import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router";
import Sidebar from "../Component/Sidebar";
import Swal from "sweetalert2";
import axios from "axios";

export default function EditDataSiswa() {
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [kelas, setKelas] = useState("");
  const [tanggalLahir, setTanggalLahir] = useState("");
  const [tempatLahir, setTempatLahir] = useState("");
  const param = useParams();
  const history = useHistory();
  const editSiswa = async (e) => {
    e.preventDefault();

    Swal.fire({
      title: "Apakah Yakin Mengubah Data?",
      text: "Data akan diubah!",
      showCancelButton: true,
      confirmButtonText: "Edit",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .put("http://localhost:2000/data-pasien/" + param.id, {
            nama: nama,
            alamat: alamat,
            tanggalLahir: tanggalLahir,
            tempatLahir: tempatLahir,
            jabatan: kelas,
          })
          .then(() => {
            history.push("/data-siswa");
            Swal.fire({
              icon: "success",
              title: "Sukses Mengubah Data",
              showConfirmButton: false,
            });
            setTimeout(() => {
              window.location.reload();
            }, 1500);
          })
          .catch((error) => {
            console.log(error);
          });
      }
    });
  };
  useEffect(() => {
    axios
      .get("http://localhost:2000/data-pasien/" + param.id)
      .then((response) => {
        const siswa = response.data.data;
        setNama(siswa.nama);
        setAlamat(siswa.alamat);
        setKelas(siswa.jabatan);
        setTanggalLahir(siswa.tanggalLahir);
        setTempatLahir(siswa.tempatLahir);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, []);

  return (
    <div>
      <Sidebar />
      <div class="sm:ml-64 overflow-auto">
        <div className="mx-48">
          <form
            className="space-y-3 md:pl-11 pl-[4.5rem] "
            onSubmit={editSiswa}
          >
            <h3 className="md:mb-4 md:py-7 py-4 md:text-2xl text-xl font-medium text-black dark:text-black">
              Edit Siswa
            </h3>

            <div className="md:flex gap-3">
              <div>
                <div>
                  <label className="block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                    Nama
                  </label>
                  <input
                    onChange={(e) => setNama(e.target.value)}
                    value={nama}
                    className="bg-white md:w-[30rem] w-[14rem] border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                    required
                  />
                </div>
                <div>
                  <label className="block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                    Kelas
                  </label>
                  <input
                    onChange={(e) => setKelas(e.target.value)}
                    value={kelas}
                    className="bg-white md:w-[30rem] w-[14rem] border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                    required
                  />
                </div>
                <div>
                  <label className="block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                    Tempat Lahir
                  </label>
                  <input
                    onChange={(e) => setTempatLahir(e.target.value)}
                    value={tempatLahir}
                    className="bg-white md:w-[30rem] w-[14rem] border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                    required
                  />
                </div>
                <div>
                  <label className="block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                    Tanggal Lahir
                  </label>
                  <input
                    onChange={(e) => setTanggalLahir(e.target.value)}
                    value={tanggalLahir}
                    type="date"
                    className="bg-white md:w-[30rem] w-[14rem] border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  />
                </div>
                <div>
                  <label className="block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                    Alamat
                  </label>
                  <textarea
                    onChange={(e) => setAlamat(e.target.value)}
                    value={alamat}
                    placeholder=" Alamat"
                    className="bg-white border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:w-[30rem] w-[14rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                    required
                  ></textarea>
                </div>
                <button
                  type="submit"
                  className="w-[5rem] md:ml-0 ml-[3rem] md:float-right rounded text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium text-sm px-4 md:py-2.5 py-1.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Simpan
                </button>
                <a
                  href="/data-siswa"
                  className="w-[5rem] md:mr-10 mr-0 rounded float-right text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium text-sm px-4 md:py-2.5 py-1.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                >
                  Batal
                </a>
              </div>

              <div></div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
