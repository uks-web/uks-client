import { React, useEffect, useState } from "react";
import axios from "axios";
import { Modal } from "react-bootstrap";
import Swal from "sweetalert2";
import bcrypt from "bcryptjs";

export default function EditProfile() {
  const [profile, setProfile] = useState([]);
  const [username, setUsername] = useState("");
  const [background, setBackground] = useState("");
  const [alamat, setAlamat] = useState("");
  const [telepon, setTelepon] = useState("");
  const [passwordType, setPasswordType] = useState("password");
  const [passwordType1, setPasswordType1] = useState("password");
  const [passwordType2, setPasswordType2] = useState("password");
  const [passLama, setPassLama] = useState("");
  const [conPassLama, setConPassLama] = useState("");
  const [passBaru, setPassBaru] = useState("");
  const [conPassBaru, setConPassBaru] = useState("");

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [show1, setShow1] = useState(false);

  const handleClose1 = () => setShow1(false);
  const handleShow1 = () => setShow1(true);
  const [show2, setShow2] = useState(false);

  const handleClose2 = () => setShow2(false);
  const handleShow2 = () => setShow2(true);

  const [show3, setShow3] = useState(false);

  const handleClose3 = () => setShow3(false);
  const handleShow3 = () => setShow3(true);

  const [show4, setShow4] = useState(false);

  const handleClose4 = () => setShow4(false);
  const handleShow4 = () => setShow4(true);

  const togglePassword = () => {
    console.log(passwordType);
    if (passwordType === "password") {
      setPasswordType("text");
      return;
    }
    setPasswordType("password");
  };

  const togglePassword1 = () => {
    if (passwordType1 === "password") {
      setPasswordType1("text");
      return;
    }
    setPasswordType1("password");
  };

  const togglePassword2 = () => {
    if (passwordType2 === "password") {
      setPasswordType2("text");
      return;
    }
    setPasswordType2("password");
  };

  const getAll = async () => {
    await axios
      .get("http://localhost:2000/uks/" + localStorage.getItem("userId"))
      .then((res) => {
        setProfile(res.data.data);
      })
      .catch((err) => {
        alert(err);
      });
  };
  useEffect(() => {
    getAll();
  }, []);
  useEffect(() => {
    axios
      .get("http://localhost:2000/uks/" + localStorage.getItem("userId"))
      .then((response) => {
        const profile = response.data.data;
        setUsername(profile.username);
        setAlamat(profile.alamat);
        setTelepon(profile.telepon);
        setPassLama(profile.password);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, []);
  const editUsername = async (e) => {
    e.preventDefault();

    Swal.fire({
      title: "Apakah Yakin Mengubah Data?",
      text: "Data akan diubah!",
      showCancelButton: true,
      confirmButtonText: "Edit",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .put(
            "http://localhost:2000/uks/update-username/" +
              localStorage.getItem("userId"),
            {
              username: username,
            }
          )
          .then(() => {
            Swal.fire({
              icon: "success",
              title: "Sukses Mengubah Data",
              showConfirmButton: false,
            });
            setTimeout(() => {
              window.location.reload();
            }, 1500);
          })
          .catch((error) => {
            console.log(error);
          });
      }
    });
  };
  const editAlamat = async (e) => {
    e.preventDefault();

    Swal.fire({
      title: "Apakah Yakin Mengubah Data?",
      text: "Data akan diubah!",
      showCancelButton: true,
      confirmButtonText: "Edit",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .put(
            "http://localhost:2000/uks/update-alamat/" +
              localStorage.getItem("userId"),
            {
              alamat: alamat,
            }
          )
          .then(() => {
            Swal.fire({
              icon: "success",
              title: "Sukses Mengubah Data",
              showConfirmButton: false,
            });
            setTimeout(() => {
              window.location.reload();
            }, 1500);
          })
          .catch((error) => {
            console.log(error);
          });
      }
    });
  };
  const editTelepon = async (e) => {
    e.preventDefault();

    Swal.fire({
      title: "Apakah Yakin Mengubah Data?",
      text: "Data akan diubah!",
      showCancelButton: true,
      confirmButtonText: "Edit",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .put(
            "http://localhost:2000/uks/update-telepon/" +
              localStorage.getItem("userId"),
            {
              telepon: telepon,
            }
          )
          .then(() => {
            Swal.fire({
              icon: "success",
              title: "Sukses Mengubah Data",
              showConfirmButton: false,
            });
            setTimeout(() => {
              window.location.reload();
            }, 1500);
          })
          .catch((error) => {
            console.log(error);
          });
      }
    });
  };
  const ubahPass = (e) => {
    e.preventDefault();
    bcrypt.compare(conPassLama, passLama, function (err, isMatch) {
      if (err) {
        throw err;
      } else if (!isMatch) {
        Swal.fire({
          icon: "error",
          title: "Password Tidak sama dengan password yang lama",
          showConfirmButton: false,
          timer: 1500,
        });
      } else {
        if (passBaru === conPassLama) {
          Swal.fire({
            icon: "error",
            title: "Password tidak boleh sama dengan sebelumnya",
            showConfirmButton: false,
            timer: 1500,
          });
        } else {
          if (passBaru === conPassBaru) {
            axios
              .put(
                "http://localhost:2000/uks/update-password/" +
                  localStorage.getItem("userId"),
                {
                  password: passBaru,
                }
              )
              .then(() => {
                Swal.fire({
                  icon: "success",
                  title: " Berhasil!!!",
                  showConfirmButton: false,
                  timer: 1500,
                });
                setTimeout(() => {
                  window.location.reload();
                }, 1500);
              })
              .catch((err) => {
                Swal.fire({
                  icon: "error",
                  title: "Password minimal 8-20 karater, angka & huruf kecil",
                  showConfirmButton: false,
                  timer: 1500,
                });
                console.log(err);
              });
          } else {
            Swal.fire({
              icon: "error",
              title: "Password Tidak sama",
              showConfirmButton: false,
              timer: 1500,
            });
          }
        }
      }
    });
  };
  const editBackground = async (e) => {
    e.preventDefault();
    e.persist();

    const data = new FormData();
    data.append("file", background);
    try {
      await axios.put(
        `http://localhost:2000/uks/update-background/${localStorage.getItem(
          "userId"
        )}`,
        data
      );
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "berhasil mengedit",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <div>
      <div className=" w-full ml-12 container  h-2/4  m-10 bg-white border border-gray-200 rounded-lg shadow sm:p-8 dark:bg-gray-800 dark:border-gray-700">
        <div className="flex items-center justify-between mb-4">
          <h5 className="text-xl font-bold leading-none text-gray-900 dark:text-white">
            Ubah Profile
          </h5>
          <a
            href="/profile"
            className="text-sm font-medium text-blue-600 hover:underline dark:text-blue-500"
          >
            Kembali
          </a>
        </div>
        <div className="flow-root">
          <ul
            role="list"
            className="divide-y divide-gray-200 dark:divide-gray-700"
          >
            <li className="py-3 sm:py-4 ">
              <div className="flex items-center space-x-4">
                <div className="flex-shrink-0"></div>
                <div className="flex-1 min-w-0">
                  <p className="text-sm font-medium text-gray-900 truncate dark:text-white">
                    Username
                  </p>
                  <p className="text-sm text-gray-500 truncate dark:text-gray-400">
                    {profile.username}
                  </p>
                </div>
                <div
                  onClick={handleShow}
                  className="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white"
                >
                  <i className="fa-solid fa-arrow-right cursor-pointer"></i>
                </div>
              </div>
            </li>
            <li className="py-3 sm:py-4 ">
              <div className="flex items-center space-x-4">
                <div className="flex-shrink-0"></div>
                <div className="flex-1 min-w-0">
                  <p className="text-sm font-medium text-gray-900 truncate dark:text-white">
                    Alamat
                  </p>
                  <p className="text-sm text-gray-500 truncate dark:text-gray-400">
                    {profile.alamat}
                  </p>
                </div>
                <div
                  onClick={handleShow1}
                  className="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white"
                >
                  <i className="fa-solid fa-arrow-right cursor-pointer"></i>
                </div>
              </div>
            </li>
            <li className="py-3 sm:py-4">
              <div className="flex items-center space-x-4">
                <div className="flex-shrink-0"></div>
                <div className="flex-1 min-w-0">
                  <p className="text-sm font-medium text-gray-900 truncate dark:text-white">
                    No Telepon
                  </p>
                  <p className="text-sm text-gray-500 truncate dark:text-gray-400">
                    {profile.telepon}
                  </p>
                </div>
                <div
                  onClick={handleShow2}
                  className="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white"
                >
                  <i className="fa-solid fa-arrow-right cursor-pointer"></i>
                </div>
              </div>
            </li>
            <li className="py-3 sm:py-4">
              <div className="flex items-center space-x-4">
                <div className="flex-shrink-0"></div>
                <div className="flex-1 min-w-0">
                  <p className="text-sm font-medium text-gray-900 truncate dark:text-white">
                    Password
                  </p>
                  <p className="text-sm text-gray-500 truncate dark:text-gray-400">
                    ********
                  </p>
                </div>
                <div
                  onClick={handleShow3}
                  className="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white"
                >
                  <i className="fa-solid fa-arrow-right cursor-pointer"></i>
                </div>
              </div>
            </li>
            <li className="py-3 sm:py-4">
              <div className="flex items-center space-x-4">
                <div className="flex-shrink-0"></div>
                <div className="flex-1 min-w-0">
                  <p className="text-sm font-medium text-gray-900 truncate dark:text-white">
                    Ubah Background
                  </p>
                </div>
                <div
                  onClick={handleShow4}
                  className="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white"
                >
                  <i className="fa-solid fa-arrow-right cursor-pointer"></i>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
      <Modal
        show={show}
        onHide={handleClose}
        id="authentication-modal"
        tabindex="-1"
        aria-hidden="true"
        className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
      >
        <div className="relative w-full h-full max-w-md md:h-auto">
          <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
            <button
              type="button"
              className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
              data-modal-hide="authentication-modal"
              onClick={handleClose}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
            <div className="px-6 py-6 lg:px-8">
              <h3 className="mb-4 md:text-2xl text-base font-medium text-black dark:text-black">
                Ubah Username
              </h3>
              <form className="space-y-3" onSubmit={editUsername}>
                <div>
                  <label className="block mb-2 text-lg font-medium text-black dark:text-black">
                    Username
                  </label>
                  <input
                    onChange={(e) => setUsername(e.target.value)}
                    value={username}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-2 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>

                <button
                  type="submit"
                  className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Simpan
                </button>
              </form>
            </div>
          </div>
        </div>
      </Modal>
      <Modal
        show={show1}
        onHide={handleClose1}
        id="authentication-modal"
        tabindex="-1"
        aria-hidden="true"
        className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
      >
        <div className="relative w-full h-full max-w-md md:h-auto">
          <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
            <button
              type="button"
              className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
              data-modal-hide="authentication-modal"
              onClick={handleClose1}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
            <div className="px-6 py-6 lg:px-8">
              <h3 className="mb-4 md:text-2xl text-base font-medium text-black dark:text-black">
                Ubah Alamat
              </h3>
              <form className="space-y-3" onSubmit={editAlamat}>
                <div>
                  <label className="block mb-2 text-lg font-medium text-black dark:text-black">
                    Alamat
                  </label>
                  <input
                    onChange={(e) => setAlamat(e.target.value)}
                    value={alamat}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-2 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>

                <button
                  type="submit"
                  className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Simpan
                </button>
              </form>
            </div>
          </div>
        </div>
      </Modal>
      <Modal
        show={show2}
        onHide={handleClose2}
        id="authentication-modal"
        tabindex="-1"
        aria-hidden="true"
        className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
      >
        <div className="relative w-full h-full max-w-md md:h-auto">
          <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
            <button
              type="button"
              className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
              data-modal-hide="authentication-modal"
              onClick={handleClose2}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
            <div className="px-6 py-6 lg:px-8">
              <h3 className="mb-4 md:text-2xl text-base font-medium text-black dark:text-black">
                Ubah No Telepon
              </h3>
              <form className="space-y-3" onSubmit={editTelepon}>
                <div>
                  <label className="block mb-2 text-lg font-medium text-black dark:text-black">
                    No Telepon
                  </label>
                  <input
                    onChange={(e) => setTelepon(e.target.value)}
                    value={telepon}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-2 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>

                <button
                  type="submit"
                  className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Simpan
                </button>
              </form>
            </div>
          </div>
        </div>
      </Modal>
      <Modal
        show={show3}
        onHide={handleClose3}
        id="authentication-modal"
        tabIndex="-1"
        aria-hidden="true"
        className="md:ml-[30%] ml-0 fixed top-0 left-0 right-0 z-50 hidden p-4 md:inset-0 h-modal md:h-full focus:outline-none focus:ring-0"
      >
        <div className="relative w-full h-full max-w-md md:h-auto focus:outline-none focus:ring-0">
          <div className="relative bg-white rounded-lg shadow">
            <button
              type="button"
              className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:text-white"
              data-modal-hide="authentication-modal"
              onClick={handleClose3}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
            <div className="px-6 py-6 lg:px-8">
              <h3 className="mb-4 text-xl font-medium text-gray-900 dark:text-black">
                Ubah Password
              </h3>
              <form onSubmit={ubahPass} className="space-y-3">
                <div>
                  <label htmlFor="password">Password Lama</label>
                  <div className="relative border-none focus:outline-none focus:ring-0">
                    <input
                      required
                      placeholder="Masukan password lama anda"
                      type={passwordType2}
                      className="md:h-5 w-full rounded-lg border p-4 pr-12 text-sm shadow-sm focus:outline-none focus:ring-0"
                      onChange={(e) => setConPassLama(e.target.value)}
                    />

                    <span
                      onClick={togglePassword2}
                      className="absolute inset-y-0 right-0 grid place-content-center px-4 focus:outline-none focus:ring-0"
                    >
                      {passwordType2 === "password" ? (
                        <>
                          <i className="fa-solid fa-eye-slash"></i>
                        </>
                      ) : (
                        <>
                          <i className="fa-solid fa-eye"></i>
                        </>
                      )}
                    </span>
                  </div>
                </div>
                <div>
                  <label htmlFor="password">Password Baru</label>
                  <div className="relative">
                    <input
                      required
                      placeholder="Masukan password baru"
                      type={passwordType}
                      className="md:h-5 w-full rounded-lg border p-4 pr-12 text-sm shadow-sm focus:outline-none focus:ring-0"
                      onChange={(e) => setPassBaru(e.target.value)}
                    />

                    <span
                      onClick={togglePassword}
                      className="absolute inset-y-0 right-0 grid place-content-center px-4  focus:outline-none focus:ring-0"
                    >
                      {passwordType === "password" ? (
                        <>
                          <i className="fa-solid fa-eye-slash"></i>
                        </>
                      ) : (
                        <>
                          <i className="fa-solid fa-eye"></i>
                        </>
                      )}
                    </span>
                  </div>
                </div>
                <div>
                  <label htmlFor="password">Konfirmasi Password</label>
                  <div className="relative">
                    <input
                      required
                      placeholder="Konfirmasi Password Baru"
                      type={passwordType1}
                      className="md:h-5 w-full rounded-lg border p-4 pr-12 text-sm shadow-sm  focus:outline-none focus:ring-0"
                      onChange={(e) => setConPassBaru(e.target.value)}
                    />

                    <span
                      onClick={togglePassword1}
                      className="absolute inset-y-0 right-0 grid place-content-center px-4  focus:outline-none focus:ring-0"
                    >
                      {passwordType1 === "password" ? (
                        <>
                          <i className="fa-solid fa-eye-slash"></i>
                        </>
                      ) : (
                        <>
                          <i className="fa-solid fa-eye"></i>
                        </>
                      )}
                    </span>
                  </div>
                </div>
                <button
                  type="submit"
                  className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Simpan
                </button>
              </form>
            </div>
          </div>
        </div>
      </Modal>
      <Modal
        show={show4}
        onHide={handleClose4}
        id="authentication-modal"
        tabindex="-1"
        aria-hidden="true"
        className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
      >
        <div className="relative w-full h-full max-w-md md:h-auto">
          <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
            <button
              type="button"
              className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
              data-modal-hide="authentication-modal"
              onClick={handleClose4}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
            <div className="px-6 py-6 lg:px-8">
              <h3 className="mb-4 md:text-2xl text-base font-medium text-black dark:text-black">
                Ubah Background
              </h3>
              <form className="space-y-3" onSubmit={editBackground}>
                <div>
                  <label className="block mb-2 text-lg font-medium text-black dark:text-black">
                    Background
                  </label>
                  <input
                    type="file"
                    onChange={(e) => setBackground(e.target.files[0])}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-2 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>

                <button
                  type="submit"
                  className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Simpan
                </button>
              </form>
            </div>
          </div>
        </div>
      </Modal>
    </div>
  );
}
