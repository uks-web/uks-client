import {
  BrowserRouter,
  Route,
  Switch,
  useLocation,
  Redirect,
} from "react-router-dom";
import Register from "./Login&Register/Register";
import Login from "./Login&Register/Login";
import Home from "./Page/Home";
import Dashboard from "./Page/Dashboard";
import DataSiswa from "./Page/DataSiswa";
import EditDataSiswa from "./Edit/EditDataSiswa";
import Diagnosa from "./Page/Diagnosa";
import EditDiagnosa from "./Edit/EditDiagnosa";
import PenangananPertama from "./Page/PenangananPertama";
import EditPenanganan from "./Edit/EditPenanganan";
import Tindakan from "./Page/Tindakan";
import EditTindakan from "./Edit/EditTindakan";
import Obat from "./Page/Obat";
import EditObat from "./Edit/EditObat";
import DataGuru from "./Page/DataGuru";
import EditDataGuru from "./Edit/EditDataGuru";
import DataKaryawan from "./Page/DataKaryawan";
import EditDataKaryawan from "./Edit/EditKaryawan";
import PeriksaPasien from "./Page/PeriksaPasien";
import StatusPeriksa from "./Page/StatusPeriksa";
import Profile from "./Page/Profile";
import EditProfile from "./Edit/EditProfile";
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route path="/register" component={Register} exact />
          <Route path="/" component={Login} exact />
          {localStorage.getItem("userId") === null ? (
            <Redirect
              to={{
                pathname: "/",
              }}
            />
          ) : (
            <>
              <Route path="/home" component={Home} exact />
              <Route path="/dashboard" component={Dashboard} exact />
              <Route path="/data-siswa" component={DataSiswa} exact />
              <Route path="/diagnosa" component={Diagnosa} exact />
              <Route path="/penanganan" component={PenangananPertama} exact />
              <Route path="/tindakan" component={Tindakan} exact />
              <Route path="/obat" component={Obat} exact />
              <Route path="/data-guru" component={DataGuru} exact />
              <Route path="/data-karyawan" component={DataKaryawan} exact />
              <Route path="/periksa" component={PeriksaPasien} exact />
              <Route path="/profile" component={Profile} exact />
              <Route path="/edit-profile" component={EditProfile} exact />
              <Route path="/edit-siswa/:id" component={EditDataSiswa} exact />
              <Route path="/edit-diagnosa/:id" component={EditDiagnosa} exact />
              <Route
                path="/edit-penanganan/:id"
                component={EditPenanganan}
                exact
              />
              <Route path="/edit-tindakan/:id" component={EditTindakan} exact />
              <Route path="/edit-obat/:id" component={EditObat} exact />
              <Route
                path="/edit-karyawan/:id"
                component={EditDataKaryawan}
                exact
              />
              <Route path="/edit-guru/:id" component={EditDataGuru} exact />
              <Route
                path="/status-periksa/:id"
                component={StatusPeriksa}
                exact
              />
            </>
          )}
        </Switch>
      </BrowserRouter>
    </div>
  );
}
export default App;
