import { React, useState, useEffect } from "react";
import Sidebar from "../Component/Sidebar";
import { useParams, useHistory } from "react-router-dom";
import axios from "axios";
import Swal from "sweetalert2";

export default function StatusPeriksa() {
  const [nama, setNama] = useState("");
  const [status, setStatus] = useState("");
  const [keluhan, setKeluhan] = useState("");
  const [penyakit, setPenyakit] = useState([]);
  const [penanganan, setPenanganan] = useState([]);
  const [tindakan, setTindakan] = useState([]);
  const [obat, setObat] = useState([]);
  const [idPenyakit, setIdPenyakit] = useState(null);
  const [idPenanganan, setIdPenanganan] = useState(null);
  const [idTindakan, setIdTindakan] = useState(null);
  const [idObat, setIdObat] = useState(null);

  const param = useParams();
  const history = useHistory();
  useEffect(() => {
    axios
      .get("http://localhost:2000/periksa/" + param.id)
      .then((response) => {
        const periksa = response.data.data;
        setNama(periksa.pasien.nama);
        setStatus(periksa.pasien.status);
        setKeluhan(periksa.keluhan);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, []);
  const getPenyakit = async () => {
    await axios
      .get("http://localhost:2000/diagnosa")
      .then((res) => {
        setPenyakit(res.data.data);
      })
      .catch((err) => {
        alert(err);
      });
  };
  const getPenanganan = async () => {
    await axios
      .get("http://localhost:2000/penanganan")
      .then((res) => {
        setPenanganan(res.data.data);
      })
      .catch((err) => {
        alert(err);
      });
  };
  const getTindakan = async () => {
    await axios
      .get("http://localhost:2000/tindakan")
      .then((res) => {
        setTindakan(res.data.data);
      })
      .catch((err) => {
        alert(err);
      });
  };
  const getObat = async () => {
    await axios
      .get("http://localhost:2000/obat")
      .then((res) => {
        setObat(res.data.data);
      })
      .catch((err) => {
        alert(err);
      });
  };
  useEffect(() => {
    getPenyakit();
    getPenanganan();
    getTindakan();
    getObat();
  }, []);
  const tangani = async (e) => {
    e.preventDefault();

    Swal.fire({
      title: "Apakah Yakin Sudah Benar?",
      text: "Data tidak akan bisa diubah lagi!",
      showCancelButton: true,
      confirmButtonText: "Benar",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .put("http://localhost:2000/periksa/tangani/" + param.id, {
            diagnosa: idPenyakit,
            obat: idObat,
            penanganan: idPenanganan,
            tindakan: idTindakan,
          })
          .then(() => {
            history.push("/periksa");
            Swal.fire({
              icon: "success",
              title: "Pasien Berhasil Di Tangani",
              showConfirmButton: false,
            });
            setTimeout(() => {
              window.location.reload();
            }, 1500);
          })
          .catch((error) => {
            console.log(error);
          });
      }
    });
  };
  return (
    <div>
      <Sidebar />
      <div className="sm:ml-64 overflow-auto">
        <div className="px-10 rounded-xl py-4">
          <div className=" text-base bg-rose-700 text-white py-2 rounded-t-xl text-center">
            <span className="text-center">Periksa Pasien : </span>
            <span>{nama.toUpperCase()}</span>
          </div>
          <div className="shadow-lg p-4">
            <div className="flex">
              <div>
                <label className="block mb-3 md:text-base text-base font-medium text-black dark:text-black">
                  Nama Pasien
                </label>
                <input
                  value={nama}
                  className="bg-gray-100 md:w-[30rem] w-[14rem]  border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-2.5 p-2.5 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  disabled
                />
              </div>
              <div className="pl-6">
                <label className="block mb-3 md:text-base text-base font-medium text-black dark:text-black">
                  Status Pasien
                </label>
                <input
                  value={status}
                  className="bg-gray-100 md:w-[30rem] w-[14rem]  border mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-2.5 p-2.5 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  disabled
                />
              </div>
            </div>
            <div>
              <label className="block mb-3 md:text-base text-sm font-medium text-black dark:text-black">
                Keluhan Pasien
              </label>
              <textarea
                value={keluhan}
                className="bg-gray-100 border  mb-4 border-gray-300 text-black text-sm focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                disabled
              ></textarea>
            </div>
            <form onSubmit={tangani}>
              <div className="flex gap-8">
                <div>
                  <label className="block mb-2 text-base font-medium text-black dark:text-black">
                    Penyakit Pasien
                  </label>
                  <select
                    required
                    onChange={(e) => setIdPenyakit(e.target.value)}
                    className="w-56 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  >
                    <option selected>Pilih Penyakit</option>
                    {penyakit.map((data) => (
                      <option value={data.id}>{data.namaPenyakit}</option>
                    ))}
                  </select>
                </div>
                <div>
                  <label className="block mb-2 text-base font-medium text-black dark:text-black">
                    Penanganan Pertama
                  </label>
                  <select
                    required
                    onChange={(e) => setIdPenanganan(e.target.value)}
                    className="w-56 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  >
                    <option selected>Pilih Penanganan</option>
                    {penanganan.map((data) => (
                      <option value={data.id}>{data.namaPenanganan}</option>
                    ))}
                  </select>
                </div>
                <div>
                  <label className="block mb-2 text-base font-medium text-black dark:text-black">
                    Tindakan
                  </label>
                  <select
                    required
                    onChange={(e) => setIdTindakan(e.target.value)}
                    className="w-56 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  >
                    <option selected>Pilih Tindakan</option>
                    {tindakan.map((data) => (
                      <option value={data.id}>{data.namaTindakan}</option>
                    ))}
                  </select>
                </div>
                <div>
                  <label className="block mb-2 text-base font-medium text-black dark:text-black">
                    Obat
                  </label>
                  <select
                    required
                    onChange={(e) => setIdObat(e.target.value)}
                    className="w-56 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  >
                    <option selected>Pilih Obat</option>
                    {obat.map((data) => (
                      <option value={data.id}>{data.namaObat}</option>
                    ))}
                  </select>
                </div>
              </div>
              <hr className="border border-gray-400 mt-24" />
              <div className="flex mt-4">
                <div className=" mt-4 ml-auto">
                  <button
                    type="submit"
                    className="text-white bg-green-500 font-semibold text-sm py-2 px-10  rounded-lg"
                  >
                    Selesai
                  </button>
                </div>
              </div>
            </form>
            <a href="/periksa">
              <div className="mt-[-35px]">
                <button className="text-white bg-red-600 font-semibold text-sm py-2 px-10  rounded-lg">
                  Kembali
                </button>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
