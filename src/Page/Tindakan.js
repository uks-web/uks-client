import React, { useState, useEffect } from "react";
import Sidebar from "../Component/Sidebar";
import { Button, Modal } from "react-bootstrap";
import Swal from "sweetalert2";
import axios from "axios";
import Navbar from "../Component/Navbar";

export default function Tindakan() {
  const [tindakan, setTindakan] = useState([]);
  const [namaTindakan, setNamaTindakan] = useState("");
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const addTindakan = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:2000/tindakan", {
        namaTindakan: namaTindakan,
      });
      //Sweet Alert
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Berhasil!!!",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    } catch (error) {
      alert("Terjadi Kesalahan " + error);
    }
  };
  const getAll = async () => {
    await axios
      .get("http://localhost:2000/tindakan")
      .then((res) => {
        setTindakan(res.data.data);
      })
      .catch((err) => {
        alert(err);
      });
  };
  useEffect(() => {
    getAll();
  }, []);
  const deleteTindakan = async (id) => {
    Swal.fire({
      title: "Apakah Anda Ingin Menghapus?",
      text: "Perubahan data tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Hapus",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:2000/tindakan/" + id);
        Swal.fire({
          icon: "success",
          title: "Dihapus!",
          showConfirmButton: false,
          timer: 1500,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    });
  };
  return (
    <div>
      <Navbar />
      <Sidebar />
      <div className="sm:ml-64 overflow-auto">
        <div className="px-10 rounded-xl py-4">
          <div className=" text-xl font-semibold bg-rose-700 text-white py-4 rounded-t-xl">
            <span className="pl-10">Daftar Tindakan</span>
            <div className="text-right mt-[-30px]">
              <button
                type="submit"
                onClick={handleShow}
                class=" text-white bg-blue-700 hover:bg-blue-800  focus:outline-none focus:ring-4 focus:ring-blue-400 font-medium rounded-lg text-sm px-5 py-2 text-center mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
              >
                <i className="fas fa-plus mr-1"> </i>
                Tambah
              </button>
            </div>
          </div>
          <table className="min-w-full  text-center rounded-xl">
            <thead className="border-b">
              <tr className="text-center">
                <th
                  scope="col"
                  className="md:text-sm text-xs pl-8 font-medium w-8 md:py-4 py-2"
                >
                  No
                </th>
                <th
                  scope="col"
                  className="md:text-sm text-xs font-medium md:py-4 py-2"
                >
                  Nama Tindakan
                </th>
                <th
                  scope="col"
                  className="md:text-sm text-xs font-medium md:py-4 py-2"
                >
                  Aksi
                </th>
              </tr>
            </thead>
            <tbody className="bg-gray-100">
              {tindakan.map((tindakan, index) => (
                <tr key={tindakan.id} className="border-b">
                  <td className="px-6 pl-12 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                    {index + 1}
                  </td>
                  <td className="px-4 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                    {tindakan.namaTindakan}
                  </td>
                  <td>
                    <a
                      a
                      href={"/edit-tindakan/" + tindakan.id}
                      style={{ marginRight: "5px" }}
                    >
                      <button className=" text-blue-500 md:text-xl text-xl font-bold py-1 px-2 rounded">
                        <i class="fa-regular fa-pen-to-square"></i>
                      </button>
                    </a>
                    <button
                      onClick={() => deleteTindakan(tindakan.id)}
                      className="text-red-500 font-bold py-1 md:text-xl text-xl px-2 rounded"
                    >
                      <i class="far fa-trash-alt"></i>
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <Modal
          show={show}
          onHide={handleClose}
          id="authentication-modal"
          tabindex="-1"
          aria-hidden="true"
          className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
        >
          <div className="relative w-full h-full max-w-md md:h-auto">
            <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
              <button
                type="button"
                className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                data-modal-hide="authentication-modal"
                onClick={handleClose}
              >
                <svg
                  aria-hidden="true"
                  className="w-5 h-5"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  ></path>
                </svg>
                <span className="sr-only">Close modal</span>
              </button>
              <div className="px-6 py-6 lg:px-8">
                <h3 className="mb-4 md:text-2xl text-base font-medium text-black dark:text-black">
                  Tambah Tindakan
                </h3>
                <form className="space-y-3" onSubmit={addTindakan}>
                  <div>
                    <label className="block mb-2 text-lg font-medium text-black dark:text-black">
                      Nama Tindakan
                    </label>
                    <input
                      onChange={(e) => setNamaTindakan(e.target.value)}
                      placeholder="Nama Tindakan"
                      className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-2 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                      required
                    />
                  </div>

                  <button
                    type="submit"
                    className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    Tambah
                  </button>
                </form>
              </div>
            </div>
          </div>
        </Modal>
      </div>
    </div>
  );
}
