import { React, useEffect, useState } from "react";
import Sidebar from "../Component/Sidebar";
import axios from "axios";
import { Modal } from "react-bootstrap";
import Swal from "sweetalert2";

export default function Profile() {
  const [profile, setProfile] = useState([]);
  const [foto, setFoto] = useState("");
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const getAll = async () => {
    await axios
      .get("http://localhost:2000/uks/" + localStorage.getItem("userId"))
      .then((res) => {
        setProfile(res.data.data);
      })
      .catch((err) => {
        alert(err);
      });
  };
  const editFoto = async (e) => {
    e.preventDefault();
    e.persist();

    const data = new FormData();
    data.append("file", foto);
    try {
      await axios.put(
        `http://localhost:2000/uks/update-foto/${localStorage.getItem(
          "userId"
        )}`,
        data
      );
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "berhasil mengedit",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getAll();
  }, []);
  return (
    <div>
      <Sidebar />
      <div className=" flex flex-wrap items-center justify-center">
        {/* Card Profile */}
        <div className=" max-w-lg bg-white rounded dark:bg-gray-800 shadow-lg transform duration-200 easy-in-out m-4">
          {/* Background */}
          <div className=" sm:h-96 overflow-hidden h-3/4">
            <img
              className="w-full rounded-t"
              src={
                profile.background !== null
                  ? profile.background
                  : "backgorund.avif"
              }
            />
          </div>
          {/* End Background */}
          {/* Foto Profile */}
          <div className="flex justify-start px-5 -mt-12 mb-5">
            <span clspanss="block relative h-32 w-32">
              <img
                src={profile.foto !== null ? profile.foto : "1-min.png"}
                className="mx-auto object-cover rounded-full h-24 w-24 bg-white p-1"
              />
              <div
                onClick={handleShow}
                className="relative mt-[-26px] ml-[11px]"
              >
                <i className="fa-solid fa-camera text-gray-100 bg-gray-700 px-7 py-0.5 rounded-bl-full rounded-br-full"></i>
              </div>
            </span>
          </div>
          {/* End Foto Profile */}
          {/* Data User */}
          <div className="">
            <div className="px-7 mb-8">
              <h2 className="text-3xl font-bold text-cyan-600 dark:text-gray-300">
                {profile.username}
              </h2>
              <p className="text-gray-400 mt-2 dark:text-gray-400">
                {profile.telepon !== null
                  ? profile.telepon
                  : "No telepon tidak terdaftar"}
              </p>
              <p className="text-gray-500 mt-2 dark:text-gray-400">
                {profile.alamat !== null
                  ? profile.alamat
                  : "Alamat tidak terdaftar"}
              </p>
              <a href="/edit-profile">
                <div className="justify-center px-4 py-2 cursor-pointer bg-gray-900 max-w-max mx-auto mt-8 rounded-lg text-gray-300 hover:bg-gray-700 hover:text-gray-100 dark:bg-gray-700 dark:text-gray-300 dark:hover:bg-gray-600 dark:hover:text-gray-200">
                  Ubah Profile
                  <span className="ml-2">
                    <i className="fa-solid fa-arrow-right cursor-pointer"> </i>
                  </span>
                </div>
              </a>
            </div>
          </div>
          {/* End Data user */}
        </div>
        {/* End Card Profile */}
      </div>
      <Modal
        show={show}
        onHide={handleClose}
        id="authentication-modal"
        tabindex="-1"
        aria-hidden="true"
        className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
      >
        <div className="relative w-full h-full max-w-md md:h-auto">
          <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
            <button
              type="button"
              className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
              data-modal-hide="authentication-modal"
              onClick={handleClose}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
            <div className="px-6 py-6 lg:px-8">
              <h3 className="mb-4 md:text-2xl text-base font-medium text-black dark:text-black">
                Ubah Foto
              </h3>
              <form className="space-y-3" onSubmit={editFoto}>
                <div>
                  <label className="block mb-2 text-lg font-medium text-black dark:text-black">
                    Ubah Foto
                  </label>
                  <input
                    type="file"
                    onChange={(e) => setFoto(e.target.files[0])}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-2 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>

                <button
                  type="submit"
                  className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Simpan
                </button>
              </form>
            </div>
          </div>
        </div>
      </Modal>
    </div>
  );
}
