import { React, useState, useEffect, useRef } from "react";
import { Modal } from "react-bootstrap";
import Sidebar from "../Component/Sidebar";
import axios from "axios";
import Swal from "sweetalert2";
import Navbar from "../Component/Navbar";
import { useDownloadExcel } from "react-export-table-to-excel";

export default function PeriksaPasien() {
  const [keluhan, setKeluhan] = useState("");
  const [show, setShow] = useState(false);
  const [nama, setNama] = useState([]);
  const [status, setStatus] = useState("");
  const [pasien, setPasien] = useState([]);
  const [idPasien, setIdpasien] = useState(null);
  const [rekapData, setRekapData] = useState([]);
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const [downloadData, setDownloadData] = useState([]);
  const [showRekapData, setShowRekapData] = useState(false);
  const [showFilter, setShowFilter] = useState(false);
  const tableRef = useRef(null);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleCloseFilter = () => setShowFilter(false);
  const handleShowFilter = () => setShowFilter(true);
  const handleShowRekapData = () => setShowRekapData(true);

  function formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [day, month, year].join("-");
  }
  const { onDownload } = useDownloadExcel({
    currentTableRef: tableRef.current,
    filename: "Data Periksa " + new Date(),
    sheet: "UKS",
  });
  console.log(tableRef.current);

  const addPasien = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:2000/periksa", {
        keluhan: keluhan,
        pasien: idPasien,
        statusPasien: status,
      });
      //Sweet Alert
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Berhasil!!!",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    } catch (error) {
      alert("Terjadi Kesalahan " + error);
    }
  };

  const allData = async (status) => {
    await axios
      .get(`http://localhost:2000/data-pasien/all?status=` + status)
      .then((res) => {
        setNama(res.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const changeStatus = (e) => {
    setStatus(e.target.value);
    allData(e.target.value);
  };

  const getAll = async () => {
    await axios
      .get("http://localhost:2000/periksa")
      .then((res) => {
        setPasien(res.data.data);
      })
      .catch((err) => {
        alert(err);
      });
  };
  const downloadExcel = async () => {
    const response = await axios.get(
      `http://localhost:2000/periksa/tanggal?end_date=${endDate}&start_date=${startDate}`
    );
    setDownloadData(response.data);
    setTimeout(() => {
      onDownload();
      Swal.fire({
        icon: "success",
        title: "File Anda Telah Di Unduh",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    }, 1000);
  };
  const handleSubmit = (e) => {
    e.preventDefault();

    axios
      .get(
        `http://localhost:2000/periksa/tanggal?end_date=${endDate}&start_date=${startDate}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      )
      .then((res) => {
        setRekapData(res.data.data);
        handleShowRekapData();
        handleCloseFilter();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const download = async (id) => {
    await Swal.fire({
      title: "Apakah Anda Yakin?",
      text: "File ini berisi data pasien!!",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, download!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios({
          url: "http://localhost:2000/periksa-excel/download?id=" + id,
          method: "GET",
          responseType: "blob",
        }).then((response) => {
          var fileURL = window.URL.createObjectURL(new Blob([response.data]));
          var fileLink = document.createElement("a");

          fileLink.href = fileURL;
          fileLink.setAttribute("download", "data-periksa-pasien.xlsx");
          document.body.appendChild(fileLink);

          fileLink.click();
        });
        Swal.fire("Download!", "File Anda Telah Di Unduh", "success");
      }
    });
  };
  useEffect(() => {
    getAll();
  }, []);
  return (
    <div>
      <Navbar />
      <Sidebar />
      <div className="sm:ml-64 overflow-auto">
        <div className=" mx-10">
          <div className="grid grid-cols-1 px-2 md:grid-cols-3 py-2.5 bg-rose-700 text-white text-xl rounded-t-lg mt-4">
            <div className="flex justify-center mb-2 md:justify-start md:pl-6">
              Filter Rekap Data
            </div>
            <div className="flex flex-wrap justify-center col-span-2 gap-2 md:justify-end">
              <button
                className=" float-end 
          text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-sky-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center  md:w-[200px]"
                onClick={handleShowFilter}
              >
                Filter Tanggal
              </button>
            </div>
          </div>
          <div className="w-full px-4 bg-white rounded-b-lg shadow">
            {showRekapData ? (
              <div className="grid mb-2 py-6 md:grid-cols-1">
                <p className="text-4xl text-center font-medium">
                  Rekap Data Pasien <br />
                  <span className="text-xl">
                    ({startDate}) - ({endDate})
                  </span>
                </p>
                <div className="flex justify-center my-2">
                  <button
                    className=" bg-blue-700
          text-white  hover:bg-sky-600 focus:ring-4 focus:outline-none focus:ring-sky-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center"
                    onClick={() => downloadExcel()}
                  >
                    Download Rekap Data Pasien
                  </button>
                </div>
              </div>
            ) : (
              <section className="flex items-center h-full ">
                <div className="container flex flex-col items-center justify-center px-5 mx-auto my-8 space-y-8 text-center sm:max-w-md">
                  <p className="text-3xl">
                    Filter terlebih dahulu sesuai tanggal yang diinginkan.
                  </p>
                </div>
              </section>
            )}
          </div>
          <div className=" rounded-xl py-4 mt-8">
            <div className=" text-xl font-semibold bg-rose-700 text-white py-4 rounded-t-xl">
              <span className="pl-10">Daftar Pasien</span>
              <div className="text-right mt-[-30px]">
                <button
                  type="submit"
                  onClick={handleShow}
                  class=" text-white bg-blue-700 hover:bg-blue-800  focus:outline-none focus:ring-4 focus:ring-blue-400 font-medium rounded-lg text-sm px-5 py-2 text-center mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  <i className="fas fa-plus mr-1"> </i>
                  Tambah
                </button>
              </div>
            </div>
            <table
              ref={tableRef}
              className="min-w-full  text-center rounded-xl"
            >
              <thead className="border-b">
                <tr className="text-center">
                  <th
                    scope="col"
                    className="md:text-sm text-xs pl-8 font-medium w-8 md:py-4 py-2"
                  >
                    No
                  </th>
                  <th
                    scope="col"
                    className="md:text-sm text-xs font-medium md:py-4 py-2"
                  >
                    Nama Pasien
                  </th>
                  <th
                    scope="col"
                    className="md:text-sm text-xs font-medium md:py-4 py-2"
                  >
                    Status Pasien
                  </th>
                  <th
                    scope="col"
                    className="md:text-sm text-xs font-medium md:py-4 py-2"
                  >
                    Tanggal
                  </th>
                  <th
                    scope="col"
                    className="md:text-sm text-xs font-medium md:py-4 py-2"
                  >
                    Keterangan
                  </th>
                  <th
                    scope="col"
                    className="md:text-sm text-xs font-medium md:py-4 py-2"
                  >
                    Obat
                  </th>
                  <th
                    scope="col"
                    className="md:text-sm text-xs font-medium md:py-4 py-2"
                  >
                    Status
                  </th>
                  <th
                    scope="col"
                    className="md:text-sm text-xs font-medium md:py-4 py-2"
                  >
                    Aksi
                  </th>
                </tr>
              </thead>
              {showRekapData ? (
                <tbody className="bg-gray-100">
                  {rekapData.map((pasien, index) => (
                    <tr key={pasien.id} className="border-b">
                      <td className="px-6 pl-12 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                        {index + 1}
                      </td>
                      <td className="px-4 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                        {pasien.pasien.nama}
                      </td>
                      <td className="px-4 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                        {pasien.pasien.status}
                      </td>
                      <td className="px-4 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                        {formatDate(pasien.tanggal)}
                      </td>
                      <td className="px-4 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                        {pasien.status === "Sudah" ? (
                          <> {pasien.tindakan.namaTindakan}</>
                        ) : (
                          <hr className="border border-gray-600 w-3 mx-auto"></hr>
                        )}
                      </td>
                      <td className="px-4 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                        {pasien.status === "Sudah" ? (
                          <> {pasien.obat.namaObat}</>
                        ) : (
                          <hr className="border border-gray-600 w-3 mx-auto"></hr>
                        )}
                      </td>
                      <td className="px-4 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                        {pasien.status === "Sudah" ? (
                          <span className="text-green-500">
                            Sudah Ditangani
                          </span>
                        ) : (
                          <span className="text-red-600">Belum Ditangani</span>
                        )}
                      </td>
                      <td>
                        {pasien.status === "Sudah" ? (
                          <div className="flex gap-2">
                            <button
                              onClick={() => download(pasien.id)}
                              className=" md:text-xl text-xl font-bold"
                            >
                              <i class="fa-solid fa-download"></i>
                            </button>
                            <button className="text-white bg-green-500 font-semibold text-sm py-2 px-4 rounded-lg">
                              Selesai
                            </button>
                          </div>
                        ) : (
                          <>
                            <a href={"/status-periksa/" + pasien.id}>
                              <button className="text-white bg-red-600 font-semibold text-sm  py-2 px-4 rounded-lg">
                                Tangani
                              </button>
                            </a>
                          </>
                        )}
                      </td>
                    </tr>
                  ))}
                </tbody>
              ) : (
                <tbody className="bg-gray-100">
                  {pasien.map((pasien, index) => (
                    <tr key={pasien.id} className="border-b">
                      <td className="px-6 pl-12 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                        {index + 1}
                      </td>
                      <td className="px-4 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                        {pasien.pasien.nama}
                      </td>
                      <td className="px-4 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                        {pasien.pasien.status}
                      </td>
                      <td className="px-4 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                        {formatDate(pasien.tanggal)}
                      </td>
                      <td className="px-4 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                        {pasien.status === "Sudah" ? (
                          <> {pasien.tindakan.namaTindakan}</>
                        ) : (
                          <hr className="border border-gray-600 w-3 mx-auto"></hr>
                        )}
                      </td>
                      <td className="px-4 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                        {pasien.status === "Sudah" ? (
                          <> {pasien.obat.namaObat}</>
                        ) : (
                          <hr className="border border-gray-600 w-3 mx-auto"></hr>
                        )}
                      </td>
                      <td className="px-4 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                        {pasien.status === "Sudah" ? (
                          <span className="text-green-500">
                            Sudah Ditangani
                          </span>
                        ) : (
                          <span className="text-red-600">Belum Ditangani</span>
                        )}
                      </td>
                      <td>
                        {pasien.status === "Sudah" ? (
                          <div className="flex gap-2">
                            <button
                              onClick={() => download(pasien.id)}
                              className=" md:text-xl text-xl font-bold"
                            >
                              <i class="fa-solid fa-download bg-blue-700 text-white p-2 rounded-lg"></i>
                            </button>
                            <button className="text-white bg-green-500 font-semibold text-sm py-2 px-4 rounded-lg">
                              Selesai
                            </button>
                          </div>
                        ) : (
                          <>
                            <a href={"/status-periksa/" + pasien.id}>
                              <button className="text-white bg-red-600 font-semibold text-sm ml-4 py-2 px-4 rounded-lg">
                                Tangani
                              </button>
                            </a>
                          </>
                        )}
                      </td>
                    </tr>
                  ))}
                </tbody>
              )}
            </table>
          </div>
        </div>
        <Modal
          show={show}
          onHide={handleClose}
          id="authentication-modal"
          tabindex="-1"
          aria-hidden="true"
          className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
        >
          <div className="relative w-full h-full max-w-md md:h-auto">
            <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
              <button
                type="button"
                className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                data-modal-hide="authentication-modal"
                onClick={handleClose}
              >
                <svg
                  aria-hidden="true"
                  className="w-5 h-5"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  ></path>
                </svg>
                <span className="sr-only">Close modal</span>
              </button>
              <div className="px-6 py-6 lg:px-8">
                <h3 className="mb-4 md:text-2xl text-base font-medium text-black dark:text-black">
                  Tambah Daftar Pasien
                </h3>
                <form className="space-y-3" onSubmit={addPasien}>
                  <div>
                    <label className="block mb-2 text-base font-medium text-black dark:text-black">
                      Status Pasien
                    </label>
                    <select
                      className="w-96 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      onChange={(e) => changeStatus(e)}
                    >
                      <option selected>Pilih Status</option>
                      <option value="Guru">Guru</option>
                      <option value="Siswa">Siswa</option>
                      <option value="Karyawan">Karyawan</option>
                    </select>
                  </div>
                  <div>
                    <label className="block mb-2 text-lg font-medium text-black dark:text-black">
                      Nama Pasien
                    </label>
                    <select
                      className="w-96 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      onChange={(e) => setIdpasien(e.target.value)}
                    >
                      <option selected>Pilih Nama</option>
                      {nama.map((pasien) => (
                        <option value={pasien.id}>{pasien.nama}</option>
                      ))}
                    </select>
                  </div>
                  <div>
                    <label className="block mb-2 text-lg font-medium text-black dark:text-black">
                      Keluhan
                    </label>
                    <input
                      onChange={(e) => setKeluhan(e.target.value)}
                      placeholder="Keluhan Pasien"
                      className="w-96 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      required
                    />
                  </div>

                  <button
                    type="submit"
                    className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    Tambah
                  </button>
                </form>
              </div>
            </div>
          </div>
        </Modal>
        <Modal
          show={showFilter}
          onHide={handleCloseFilter}
          id="authentication-modal"
          tabindex="-1"
          aria-hidden="true"
          className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
        >
          <div className="relative w-full h-full max-w-md md:h-auto">
            <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
              <button
                type="button"
                className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                data-modal-hide="authentication-modal"
                onClick={handleCloseFilter}
              >
                <svg
                  aria-hidden="true"
                  className="w-5 h-5"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  ></path>
                </svg>
                <span className="sr-only">Close modal</span>
              </button>
              <div className="px-6 py-6 lg:px-8">
                <h3 className="mb-4 md:text-2xl text-base font-medium text-black dark:text-black">
                  Filter Rekap Data Pasien
                </h3>
                <form className="space-y-3" onSubmit={handleSubmit}>
                  <div>
                    <label className="block mb-2 text-base font-medium text-black dark:text-black">
                      Dari Tanggal :
                    </label>
                    <input
                      type="date"
                      onChange={(e) => setStartDate(e.target.value)}
                      className="w-96 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      required
                    />
                  </div>
                  <div>
                    <label className="block mb-2 text-lg font-medium text-black dark:text-black">
                      Sampai Tanggal :
                    </label>
                    <input
                      type="date"
                      onChange={(e) => setEndDate(e.target.value)}
                      className="w-96 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      required
                    />
                  </div>

                  <button
                    type="submit"
                    className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    Tampilkan
                  </button>
                </form>
              </div>
            </div>
          </div>
        </Modal>
      </div>
    </div>
  );
}
