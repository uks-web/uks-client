import { React, useState, useEffect } from "react";
import Sidebar from "../Component/Sidebar";
import axios from "axios";
import Navbar from "../Component/Navbar";

export default function Dashboard() {
  const [pasien, setPasien] = useState([]);
  const [guru, setGuru] = useState([]);
  const [siswa, setSiswa] = useState([]);
  const [karyawan, setKaryawan] = useState([]);
  function formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [day, month, year].join("-");
  }
  const getAll = async () => {
    await axios
      .get("http://localhost:2000/periksa")
      .then((res) => {
        setPasien(res.data.data);
      })
      .catch((err) => {
        alert(err);
      });
  };
  const getAllGuru = async () => {
    await axios
      .get("http://localhost:2000/periksa/all?status_pasien=Guru")
      .then((res) => {
        setGuru(res.data.data);
      })
      .catch((err) => {
        alert(err);
      });
  };
  const getAllSiswa = async () => {
    await axios
      .get("http://localhost:2000/periksa/all?status_pasien=Siswa")
      .then((res) => {
        setSiswa(res.data.data);
      })
      .catch((err) => {
        alert(err);
      });
  };
  const getAllKaryawan = async () => {
    await axios
      .get("http://localhost:2000/periksa/all?status_pasien=Karyawan")
      .then((res) => {
        setKaryawan(res.data.data);
      })
      .catch((err) => {
        alert(err);
      });
  };
  useEffect(() => {
    getAll();
    getAllGuru();
    getAllSiswa();
    getAllKaryawan();
  }, []);
  return (
    <div>
      <Navbar />
      <Sidebar />
      <div className="sm:ml-64 overflow-auto mt-[-10px]">
        <div className="container mx-auto py-8 p-10">
          <div className="grid grid-cols-1 gap-12 mb-2 lg:grid-cols-3">
            <a>
              <div className="w-full px-4 py-5 bg-white rounded-lg shadow-xl">
                <div className="text-lg font-medium truncate text-center">
                  Daftar Pasien Guru
                </div>
                <div className="mt-1 text-3xl font-semibold text-center text-rose-800">
                  <button className="inlne-block mx-1 h-14 w-14 rounded-full bg-rose-700 hover:bg-rose-800 uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca]">
                    <i className="fa-solid fa-hospital-user"></i>
                  </button>
                  {guru.length}
                  <span> Guru</span>
                </div>
              </div>
            </a>

            <a>
              <div className="w-full px-4 py-5 bg-white rounded-lg shadow-xl">
                <div className="text-lg font-medium truncate text-center">
                  Daftar Pasien Siswa
                </div>
                <div className="mt-1 text-3xl font-semibold text-center text-rose-800">
                  <button className="inlne-block mx-1 h-14 w-14 rounded-full bg-rose-700 hover:bg-rose-800 uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca]">
                    <i className="fa-solid fa-hospital-user"></i>
                  </button>
                  {siswa.length}
                  <span> Siswa</span>
                </div>
              </div>
            </a>
            <a>
              <div className="w-full px-4 py-5 bg-white rounded-lg shadow-xl">
                <div className="text-lg font-medium truncate text-center">
                  Daftar Pasien Karyawan
                </div>
                <div className="mt-1 text-3xl font-semibold text-center text-rose-800">
                  <button className="inlne-block mx-1 h-14 w-14 rounded-full bg-rose-700 hover:bg-rose-800 uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca]">
                    <i className="fa-solid fa-hospital-user"></i>
                  </button>
                  {karyawan.length}
                  <span> Karyawan</span>
                </div>
              </div>
            </a>
          </div>
        </div>
        <div className="px-10 rounded-xl">
          <div className="text-center text-xl font-semibold bg-rose-700 text-white py-2 rounded-t-xl">
            Riwayat Pasien
          </div>
          <table className="min-w-full  text-center rounded-xl">
            <thead className="border-b">
              <tr className="text-center">
                <th
                  scope="col"
                  className="md:text-sm text-xs font-medium w-8 md:py-4 py-2"
                >
                  No
                </th>
                <th
                  scope="col"
                  className="md:text-sm text-xs font-medium md:py-4 py-2"
                >
                  Nama Pasien
                </th>
                <th
                  scope="col"
                  className="md:text-sm text-xs font-medium md:py-4 py-2"
                >
                  Status Pasien
                </th>
                <th
                  scope="col"
                  className="md:text-sm text-xs font-medium md:py-4 py-2"
                >
                  Tanggal Periksa
                </th>
              </tr>
            </thead>
            <tbody className="bg-gray-100">
              {pasien.map((data, index) => (
                <tr key={pasien.id} className="border-b">
                  <td className="px-6 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                    {index + 1}
                  </td>
                  <td className="px-4 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                    {data.pasien.nama}
                  </td>
                  <td className="px-4 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                    {data.pasien.status}
                  </td>
                  <td>{formatDate(data.tanggal)}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
