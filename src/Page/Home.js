import React from "react";
import Sidebar from "../Component/Sidebar";
import Navbar from "../Component/Navbar";
export default function Home() {
  return (
    <div>
      <Navbar />
      <Sidebar />
      <div class=" sm:ml-64 overflow-auto ">
        <section class="relative bg-[url(https://jiyofullest.com/wp-content/uploads/2019/04/Home-11-Banner-Background-Image.jpg)] bg-cover bg-center bg-no-repeat">
          <div class="absolute inset-0 bg-white/75 sm:bg-transparent sm:from-white/95 sm:to-white/25 ltr:sm:bg-gradient-to-r rtl:sm:bg-gradient-to-l"></div>

          <div class="relative mx-auto max-w-screen-xl px-4 py-32 sm:px-6 lg:flex lg:h-screen lg:items-center lg:px-8">
            <div class="max-w-xl text-center ltr:sm:text-left rtl:sm:text-right">
              <h1 class="text-3xl font-extrabold sm:text-5xl">
                Selamat Datang
                <strong class="block font-extrabold text-rose-700">
                  {localStorage.getItem("username")}
                </strong>
              </h1>

              <p class="mt-4 max-w-lg sm:text-xl/relaxed">
                Mari kita bersama membangun masa depan yang sehat dan bersih
              </p>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
}
