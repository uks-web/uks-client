import React, { useState, useEffect } from "react";
import Sidebar from "../Component/Sidebar";
import { Button, Modal } from "react-bootstrap";
import Swal from "sweetalert2";
import axios from "axios";
import Navbar from "../Component/Navbar";

export default function DataKaryawan() {
  const [karyawan, setKaryawan] = useState([]);
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [tanggalLahir, setTanggalLahir] = useState("");
  const [tempatLahir, setTempatLahir] = useState("");
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [showExcel, setShowExcel] = useState(false);
  const [loading, setLoading] = useState(false);
  const [excel, setExcel] = useState("");
  const handleCloseExcel = () => setShowExcel(false);
  const handleShowExcel = () => setShowExcel(true);

  const addKaryawan = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:2000/data-pasien", {
        nama: nama,
        tanggalLahir: tanggalLahir,
        tempatLahir: tempatLahir,
        alamat: alamat,
        jabatan: "Karyawan",
        status: "Karyawan",
      });
      //Sweet Alert
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Berhasil Menambahkan Karyawan",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    } catch (error) {
      alert("Terjadi Kesalahan " + error);
    }
  };
  const getAll = async () => {
    await axios
      .get("http://localhost:2000/data-pasien/karyawan")
      .then((res) => {
        setKaryawan(res.data.data);
      })
      .catch((err) => {
        alert(err);
      });
  };
  useEffect(() => {
    getAll();
  }, []);
  function formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [day, month, year].join("-");
  }
  const deleteKaryawan = async (id) => {
    Swal.fire({
      title: "Apakah Anda Ingin Menghapus?",
      text: "Perubahan data tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Hapus",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:2000/data-pasien/" + id);
        Swal.fire({
          icon: "success",
          title: "Dihapus!",
          showConfirmButton: false,
          timer: 1500,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    });
  };

  const download = async () => {
    await Swal.fire({
      title: "Apakah Anda Yakin?",
      text: "File ini berisi semua data karyawan!!",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, download!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios({
          url: "http://localhost:2000/pasien-excel/download/karyawan",
          method: "GET",
          responseType: "blob",
        }).then((response) => {
          var fileURL = window.URL.createObjectURL(new Blob([response.data]));
          var fileLink = document.createElement("a");

          fileLink.href = fileURL;
          fileLink.setAttribute("download", "data-karyawan.xlsx");
          document.body.appendChild(fileLink);

          fileLink.click();
        });
        Swal.fire("Download!", "File Anda Telah Di Unduh", "success");
      }
    });
  };
  const downloadFormat = async () => {
    await Swal.fire({
      title: "Yakin ingin mendownload?",
      text: "Ini adalah file format excel untuk mengimport data.",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#0b409c",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, download!",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios({
          url: `http://localhost:2000/pasien-excel/download/template
          `,
          method: "GET",
          responseType: "blob",
        }).then((response) => {
          setLoading(true);
          setTimeout(() => {
            var fileURL = window.URL.createObjectURL(new Blob([response.data]));
            var fileLink = document.createElement("a");

            fileLink.href = fileURL;
            fileLink.setAttribute("download", "format-karyawan.xlsx");
            document.body.appendChild(fileLink);

            fileLink.click();
            handleCloseExcel();
            setLoading(false);
          }, 2000);
        });
      }
    });
  };
  const importExcel = async (e) => {
    e.preventDefault();

    const formData = new FormData();

    formData.append("file", excel);
    formData.append("nama", nama);
    formData.append("tempat_lahir", tempatLahir);
    formData.append("tanggal_lahir", tanggalLahir);
    formData.append("alamat", alamat);
    formData.append("jabatan", "Karyawan");
    formData.append("status", "Karyawan");

    await axios
      .post(`http://localhost:2000/pasien-excel/upload`, formData)
      .then(() => {
        Swal.fire({
          icon: "success",
          title: "Berhasil Di Tambahkan",
          showConfirmButton: false,
          timer: 1500,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
        Swal.fire("Error", "Anda belum memilih file untuk diimport!.", "error");
      });
  };
  return (
    <div>
      <Navbar />
      <Sidebar />
      <div className="sm:ml-64 overflow-auto mt-[-20px]">
        <div className="px-10 rounded-xl py-6">
          <div className=" text-xl font-semibold bg-rose-700 text-white py-4 rounded-t-xl">
            <span className="pl-10">Daftar Karyawan</span>
            <div className="text-right mt-[-30px]">
             
              <button
                onClick={handleShowExcel}
                type="button"
                class="text-white bg-blue-700 hover:bg-blue-800 focus:outline-none focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2 text-center mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
              >
                <i className="fas fa-upload mr-1"></i>
                Import Data
              </button>
              <button
                onClick={download}
                class="text-white bg-blue-700 hover:bg-blue-800 focus:outline-none focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2 text-center mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
              >
                <i className="fas fa-download mr-1"></i>
                Download Data
              </button>
              <button
                type="submit"
                onClick={handleShow}
                class=" text-white bg-blue-700 hover:bg-blue-800  focus:outline-none focus:ring-4 focus:ring-blue-400 font-medium rounded-lg text-sm px-5 py-2 text-center mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
              >
                <i className="fas fa-plus mr-1"> </i>
                Tambah
              </button>
            </div>
          </div>
          <table className="min-w-full  text-center rounded-xl">
            <thead className="border-b">
              <tr className="text-center">
                <th
                  scope="col"
                  className="md:text-sm text-xs font-medium w-8 md:py-4 py-2"
                >
                  No
                </th>
                <th
                  scope="col"
                  className="md:text-sm text-xs font-medium md:py-4 py-2"
                >
                  Nama Karyawan
                </th>
                <th
                  scope="col"
                  className="md:text-sm text-xs font-medium md:py-4 py-2"
                >
                  Tempat Tanggal Lahir
                </th>
                <th
                  scope="col"
                  className="md:text-sm text-xs font-medium md:py-4 py-2"
                >
                  Alamat
                </th>
                <th
                  scope="col"
                  className="md:text-sm text-xs font-medium md:py-4 py-2"
                >
                  Aksi
                </th>
              </tr>
            </thead>
            <tbody className="bg-gray-100">
              {karyawan.map((karyawan, index) => (
                <tr key={karyawan.id} className="border-b">
                  <td className="px-6 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                    {index + 1}
                  </td>
                  <td className="px-4 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                    {karyawan.nama}
                  </td>
                  <td className="px-4 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                    {karyawan.tempatLahir} {formatDate(karyawan.tanggalLahir)}
                  </td>
                  <td className="px-4 py-4 whitespace-nowrap md:text-sm text-xs font-medium text-gray-900 ">
                    {karyawan.alamat}
                  </td>
                  <td>
                    <a
                      a
                      href={"/edit-karyawan/" + karyawan.id}
                      style={{ marginRight: "5px" }}
                    >
                      <button className=" text-blue-500 md:text-xl text-xl font-bold py-1 px-2 rounded">
                        <i class="fa-regular fa-pen-to-square"></i>
                      </button>
                    </a>
                    <button
                      onClick={() => deleteKaryawan(karyawan.id)}
                      className="text-red-500 font-bold py-1 md:text-xl text-xl px-2 rounded"
                    >
                      <i class="far fa-trash-alt"></i>
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <Modal
          show={show}
          onHide={handleClose}
          id="authentication-modal"
          tabindex="-1"
          aria-hidden="true"
          className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
        >
          <div className="relative w-full h-full max-w-md md:h-auto">
            <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
              <button
                type="button"
                className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                data-modal-hide="authentication-modal"
                onClick={handleClose}
              >
                <svg
                  aria-hidden="true"
                  className="w-5 h-5"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  ></path>
                </svg>
                <span className="sr-only">Close modal</span>
              </button>
              <div className="px-6 py-6 lg:px-8">
                <h3 className="mb-4 md:text-2xl text-base font-medium text-black dark:text-black">
                  Tambah Karyawan
                </h3>
                <form onSubmit={addKaryawan} className="space-y-3">
                  <div>
                    <label className="block mb-2 text-lg font-medium text-black dark:text-black">
                      Nama Karyawan
                    </label>
                    <input
                      onChange={(e) => setNama(e.target.value)}
                      placeholder="Nama"
                      className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-2 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                      required
                    />
                  </div>
                  <div>
                    <label className="block mb-2 text-lg font-medium text-black dark:text-black">
                      Tempat Lahir
                    </label>
                    <input
                      onChange={(e) => setTempatLahir(e.target.value)}
                      placeholder="Tempat Lahir"
                      className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                      required
                    />
                  </div>
                  <div>
                    <label className="block mb-2 text-lg font-medium text-black dark:text-black">
                      Tanggal Lahir
                    </label>
                    <input
                      onChange={(e) => setTanggalLahir(e.target.value)}
                      type="date"
                      className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                      required
                    />
                  </div>
                  <div>
                    <label className="block mb-2 text-lg font-medium text-black dark:text-black">
                      Alamat
                    </label>
                    <input
                      onChange={(e) => setAlamat(e.target.value)}
                      placeholder="Alamat"
                      className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                      required
                    />
                  </div>
                  <button
                    type="submit"
                    className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    Tambah
                  </button>
                </form>
              </div>
            </div>
          </div>
        </Modal>
        <Modal
          show={showExcel}
          onHide={handleCloseExcel}
          id="authentication-modal"
          tabindex="-1"
          aria-hidden="true"
          className="md:ml-[30%] ml-0 fixed top-0 left-0 right-0 z-50 hidden md:w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full outline-none ring-0"
        >
          <div className="relative w-full h-full max-w-md md:h-auto">
            <div className="relative bg-white rounded-lg shadow dark:bg-gray-700">
              <button
                type="button"
                className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                data-modal-hide="authentication-modal"
                onClick={handleCloseExcel}
              >
                <svg
                  aria-hidden="true"
                  className="w-5 h-5"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  ></path>
                </svg>
                <span className="sr-only">Close modal</span>
              </button>
              <div className="px-6 py-6 lg:px-8">
                <h3 className="mb-4 text-xl font-medium text-gray-900 dark:text-white">
                  Import Karyawan Dari File Excel
                </h3>
                <div className="border border-gray-300 rounded-3xl text-center">
                  <p className="mb-4 text-center pt-4">
                    download file dibawah untuk menginput data karyawan
                  </p>
                  <p className="mt-[-15px] pb-2 font-semibold">
                    ( *column tanggal lahir diubah menjadi short date )
                  </p>
                  <button
                    className="bg-rose-600
              text-white hover:bg-rose-700 mb-4
              focus:ring-4 focus:outline-none focus:ring-green-300 
              font-medium rounded-lg text-md w-full sm:w-auto px-2 py-1 text-center"
                    onClick={downloadFormat}
                  >
                    Download Template Excel
                  </button>
                </div>
                <form onSubmit={importExcel}>
                  <div>
                    <label className="block mb-2 mt-4 font-bold text-lg  text-gray-900 dark:text-white">
                      Drop File.xlsx
                    </label>
                    <input
                      className="w-full rounded-lg border p-2 text-sm shadow-sm"
                      required
                      autoComplete="off"
                      type="file"
                      accept=".xlsx"
                      onChange={(e) => setExcel(e.target.files[0])}
                    />
                  </div>
                  <button
                    className="bg-blue-700
              text-white hover:bg-blue-800 
              focus:ring-4 focus:outline-none focus:ring-green-300 
              font-medium rounded-lg text-md w-full sm:w-auto px-4 py-2 mt-4 text-right"
                    type="submit"
                  >
                    Simpan
                  </button>
                </form>
              </div>
            </div>
          </div>
        </Modal>
      </div>
    </div>
  );
}
