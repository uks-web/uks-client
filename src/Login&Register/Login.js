import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import axios from "axios";

export default function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [passwordType, setPasswordType] = useState("password");
  const [passwordIcon, setPasswordIcon] = useState("fa-solid fa-eye-slash");

  const togglePassword = () => {
    if (passwordType === "password") {
      setPasswordType("text");
      setPasswordIcon("fa-solid fa-eye");
      return;
    }
    setPasswordType("password");
    setPasswordIcon("fa-solid fa-eye-slash");
  };
  const history = useHistory();
  const login = async (e) => {
    e.preventDefault();

    try {
      const { data, status } = await axios.post(
        "http://localhost:2000/uks/login",
        {
          username: username,
          password: password,
        }
      );
      // Jika respon 200/ ok
      if (status === 200) {
        Swal.fire({
          icon: "success",
          title: "Login Berhasil!!!",
          showConfirmButton: false,
          timer: 1500,
        });
        localStorage.setItem("userId", data.data.user.id);
        localStorage.setItem("username", data.data.user.username);
        setTimeout(() => {
          history.push("/home");
          window.location.reload();
        }, 1500);
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Username atau password tidak valid!",
        showConfirmButton: false,
        timer: 1500,
      });
      console.log(error);
    }
  };

  return (
    <div className="">
      <section className="h-screen flex flex-col md:flex-row justify-center space-y-10 md:space-y-0 md:space-x-16 items-center my-2 mx-5 md:mx-0 md:my-0">
        <div className="md:w-1/3 max-w-sm">
          <img
            src="https://tecdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.webp"
            alt="Sample image"
          />
        </div>
        <div className="md:w-1/3 max-w-sm">
          <div className="text-center md:text-left">
            <button
              type="button"
              className="inlne-block mx-1 h-9 w-9 rounded-xl bg-rose-700 hover:bg-rose-800 uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca]"
            >
              <i class="fa-solid fa-notes-medical"></i>
            </button>
            <label className="mr-1 text-xl font-semibold">
              Sistem Aplikasi UKS
            </label>
          </div>
          <div className="my-5 flex items-center before:mt-0.5 before:flex-1 before:border-t before:border-neutral-300 after:mt-0.5 after:flex-1 after:border-t after:border-neutral-300"></div>
          <form onSubmit={login}>
            <div>
              <label className="sr-only">Username</label>

              <div className="relative mb-6">
                <input
                  type="text"
                  className="w-full rounded-lg border p-4 pr-12 text-sm shadow-sm"
                  placeholder="Masukan username"
                  required
                  onChange={(e) => setUsername(e.target.value)}
                />
              </div>
            </div>

            <div>
              <label htmlFor="password" className="sr-only">
                Password
              </label>

              <div className="relative">
                <input
                  type={passwordType}
                  className="w-full rounded-lg border p-4 pr-12 text-sm shadow-sm"
                  placeholder="Masukan password"
                  onChange={(e) => setPassword(e.target.value)}
                  required
                />

                <span
                  onClick={togglePassword}
                  className="absolute inset-y-0 right-0 grid place-content-center px-4"
                >
                  <i class={passwordIcon}></i>
                </span>
              </div>
            </div>
            <div className="text-center md:text-left">
              <button
                className="mt-4 bg-rose-600 hover:bg-rose-700 px-4 py-2 text-white uppercase rounded text-xs tracking-wider"
                type="submit"
              >
                Masuk
              </button>
            </div>
          </form>
        </div>
      </section>
    </div>
  );
}
