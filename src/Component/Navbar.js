import axios from "axios";
import { React, useState, useEffect } from "react";

export default function Navbar() {
  const [profile, setProfile] = useState([]);
  const getAll = async () => {
    await axios
      .get("http://localhost:2000/uks/" + localStorage.getItem("userId"))
      .then((res) => {
        setProfile(res.data.data);
      })
      .catch((err) => {
        alert(err);
      });
  };
  useEffect(() => {
    getAll();
  }, []);
  return (
    <div className=" ml-64">
      <div className=" md:w-full h-[3rem] text-white flex">
        <div className="flex gap-2 ml-auto md:ml-auto pr-4">
          <a href="/profile">
            <img
              className="h-10 w-10 bg-white rounded-full mt-1 -ml-[3%]"
              src={profile.foto !== null ? profile.foto : "1-min.png"}
              alt=""
            />
          </a>
          <p className="mt-3 md:mt-3 text-base font-semibold md:text-base text-black">
            {localStorage.getItem("username")}
          </p>
        </div>
      </div>
    </div>
  );
}
