import { React, useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";

export default function () {
  const history = useHistory();
  const [date, setDate] = useState(new Date());
  function refreshClock() {
    setDate(new Date());
  }
  useEffect(() => {
    const timerId = setInterval(refreshClock, 1000);
    return function cleanup() {
      clearInterval(timerId);
    };
  }, []);
  function formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = "" + month;
    if (day.length < 2) day = "" + day;

    return [day, month, year].join("/");
  }
  const logout = () => {
    Swal.fire({
      title: "Anda Yakin Ingin Logout",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          icon: "success",
          title: "Berhasil Logout",
          showConfirmButton: false,
          timer: 1500,
        });
        //Untuk munuju page selanjutnya
        history.push("/");
        setTimeout(() => {
          window.location.reload();
        }, 1500);
        localStorage.clear();
      }
    });
  };

  return (
    <div className="">
      <aside
        id="sidebar-multi-level-sidebar"
        className="fixed top-0 left-0 z-40 w-64 h-screen transition-transform -translate-x-full sm:translate-x-0"
        aria-label="Sidebar"
      >
        <div className="h-full px-3 py-4 bg-gray-800 dark:bg-gray-800">
          <span className="text-xl font-bold text-gray-100">
            SISTEM APLIKASI UKS SMK BINA NUSANTARA
          </span>
          <div className="my-5 flex items-center before:mt-0.5 before:flex-1 before:border-t before:border-neutral-300 after:mt-0.5 after:flex-1 after:border-t after:border-neutral-300"></div>
          <div>
            <ul className="space-y-2 font-medium ">
              <li>
                <a
                  href="/dashboard"
                  className="flex items-center p-2 focus:bg-rose-700 text-gray-50 rounded-lg dark:text-white hover:bg-rose-700 dark:hover:bg-gray-700"
                >
                  <svg
                    aria-hidden="true"
                    className="w-6 h-6 text-gray-50 transition duration-75 dark:text-gray-400 group-hover:text-gray-900 dark:group-hover:text-white"
                    fill="currentColor"
                    viewBox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path d="M2 10a8 8 0 018-8v8h8a8 8 0 11-16 0z"></path>
                    <path d="M12 2.252A8.014 8.014 0 0117.748 8H12V2.252z"></path>
                  </svg>
                  <span className="ml-3">Dashboard</span>
                </a>
              </li>
              <li>
                <a
                  href="/periksa"
                  className="flex items-center p-2 text-gray-50 rounded-lg dark:text-white hover:bg-rose-700 dark:hover:bg-gray-700"
                >
                  <i className="fa-solid fa-user-doctor flex-shrink-0 w-6 h-6 text-gray-50 transition duration-75 dark:text-gray-50 group-hover:text-gray-500 dark:group-hover:text-white"></i>
                  <span className="flex-1 ml-3 whitespace-nowrap">
                    Periksa Pasien
                  </span>
                </a>
              </li>
              <li>
                <details className="group [&_summary::-webkit-details-marker]:hidden">
                  <summary className="flex cursor-pointer items-center justify-between rounded-lg text-gray-50 hover:bg-rose-700 hover:text-gray-50">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="w-6 h-6 ml-2 text-gray-50 transition duration-75 dark:text-gray-50 group-hover:text-gray-50 dark:group-hover:text-white"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      strokeWidth="2"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z"
                      />
                    </svg>

                    <span className="  items-center py-2 ml-[-120px] text-gray-50 rounded-lg dark:text-white hover:bg-rose-700 dark:hover:bg-rose-700">
                      Data
                    </span>

                    <span className="shrink-0 transition duration-300 group-open:-rotate-180">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-5 w-5"
                        viewBox="0 0 20 20"
                        fill="currentColor"
                      >
                        <path
                          filRule="evenodd"
                          d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                          cliRule="evenodd"
                        />
                      </svg>
                    </span>
                  </summary>

                  <nav
                    aria-label="Teams Nav"
                    className="mt-2 flex flex-col px-4"
                  >
                    <a
                      href="/data-guru"
                      className="flex items-center p-2 text-gray-50 rounded-lg dark:text-white hover:bg-rose-700 dark:hover:bg-rose-700"
                    >
                      <svg
                        aria-hidden="true"
                        className="flex-shrink-0 w-6 h-6 text-gray-50 transition duration-75 dark:text-gray-50 group-hover:text-gray-50 dark:group-hover:text-white"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          fillRule="evenodd"
                          d="M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z"
                          clipRule="evenodd"
                        ></path>
                      </svg>
                      <span className="ml-3">Daftar Guru</span>
                    </a>
                    <a
                      href="/data-siswa"
                      className="flex items-center p-2 text-gray-50 rounded-lg dark:text-white hover:bg-rose-700 dark:hover:bg-rose-700"
                    >
                      <svg
                        aria-hidden="true"
                        className="flex-shrink-0 w-6 h-6 text-gray-50 transition duration-75 dark:text-gray-50 group-hover:text-gray-50 dark:group-hover:text-white"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          fillRule="evenodd"
                          d="M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z"
                          clipRule="evenodd"
                        ></path>
                      </svg>
                      <span className="ml-3">Daftar Siswa</span>
                    </a>
                    <a
                      href="/data-karyawan"
                      className="flex items-center p-2 text-gray-50 rounded-lg dark:text-white hover:bg-rose-700 dark:hover:bg-rose-700"
                    >
                      <svg
                        aria-hidden="true"
                        className="flex-shrink-0 w-6 h-6 text-gray-50 transition duration-75 dark:text-gray-50 group-hover:text-gray-50 dark:group-hover:text-white"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          fillRule="evenodd"
                          d="M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z"
                          clipRule="evenodd"
                        ></path>
                      </svg>
                      <span className="ml-3">Daftar Karyawan</span>
                    </a>
                  </nav>
                </details>
              </li>
              <li>
                <a
                  href="/diagnosa"
                  className="flex items-center p-2 text-gray-50 rounded-lg dark:text-white hover:bg-rose-700 dark:hover:bg-rose-700"
                >
                  <i className="fa-solid fa-person-dots-from-line flex-shrink-0 w-6 h-6 text-gray-50 transition duration-75 dark:text-gray-50 group-hover:text-gray-500 dark:group-hover:text-white"></i>
                  <span className="flex-1 ml-3 whitespace-nowrap">
                    Diagnosa
                  </span>
                </a>
              </li>
              <li>
                <a
                  href="/penanganan"
                  className="flex items-center p-2 text-gray-50 rounded-lg dark:text-white hover:bg-rose-700 dark:hover:bg-rose-700"
                >
                  <i className="fa-solid fa-briefcase-medical flex-shrink-0 w-6 h-6 text-gray-50 transition duration-75 dark:text-gray-400 group-hover:text-gray-900 dark:group-hover:text-white"></i>
                  <span className="flex-1 ml-3 whitespace-nowrap">
                    Penanganan Pertama
                  </span>
                </a>
              </li>
              <li>
                <a
                  href="/tindakan"
                  className="flex items-center p-2 text-gray-50 rounded-lg dark:text-white hover:bg-rose-700 dark:hover:bg-rose-700"
                >
                  <i className="fa-solid fa-location-crosshairs flex-shrink-0 w-6 h-6 text-gray-50 transition duration-75 dark:text-gray-400 group-hover:text-gray-900 dark:group-hover:text-white"></i>
                  <span className="flex-1 ml-3 whitespace-nowrap">
                    Tindakan{" "}
                  </span>
                </a>
              </li>
              <li>
                <a
                  href="/obat"
                  className="flex items-center p-2 text-gray-50 rounded-lg dark:text-white hover:bg-rose-700 dark:hover:bg-rose-700"
                >
                  <i className="fa-solid fa-suitcase-medical flex-shrink-0 w-6 h-6 text-gray-50 transition duration-75 dark:text-gray-400 group-hover:text-gray-900 dark:group-hover:text-white"></i>
                  <span className="flex-1 ml-3 whitespace-nowrap">
                    Daftar Obat P3K{" "}
                  </span>
                </a>
              </li>
            </ul>
          </div>
          <div className=" m-12 ml-14 ">
            <p className="text-base text-center text-white ml-[-25px]">
              {" "}
              {formatDate(new Date())}
            </p>
            <span className="text-xl text-center text-white">
              {date.toLocaleTimeString()}
            </span>
          </div>
        </div>

        <div onClick={logout} className="sticky inset-x-0 bottom-0 px-4 py-2 z-50">
          <a
            href="#"
            className="flex items-center p-2 text-gray-50 rounded-lg dark:text-white hover:bg-rose-700 dark:hover:bg-rose-700"
          >
            <i className="fa-solid fa-right-from-bracket"></i>
            <span className="flex-1 ml-3 text-lg font-semibold whitespace-nowrap">
              Keluar
            </span>
          </a>
        </div>
      </aside>
    </div>
  );
}
